<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::post('/welcome0', 'AuthController@signup');

Route::get('/register', 'AuthController@namalengkap');

Route::get('/table', 'IndexController@table');

Route::get('/data-tables', 'IndexController@dt');

//CRUD CAST: 
    //index
Route::get('/cast', 'CastController@index');
    //create
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
    //show detail
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
    //delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');

