@extends('layout.master');

@section('judul')
    Detail cast {{$cast->name}}
@endsection

@section('content')
    <h1>Nama : {{$cast->name}}</h1>
    <p>Bio : {{$cast->bio}}</p>
@endsection