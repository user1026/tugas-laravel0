@extends('layout.master');

@section('judul')
    Halaman Form
@endsection

@section('content')
<h2>Tambah Cast</h2>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama:</label>
            <input type="text" class="form-control" name="name" placeholder="Masukkan Title">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur:</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio:</label>
            <textarea class="form-control" name="bio" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection