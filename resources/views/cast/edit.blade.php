@extends('layout.master');

@section('judul')
    Edit Data  Cast {{$cast->name}}
@endsection

@section('content')
<h2>Tambah Cast</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama:</label>
            <input type="text" class="form-control" name="name" value="{{$cast->name}}" placeholder="Masukkan Title">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur:</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio:</label>
            <textarea class="form-control" name="bio" value="{{$cast->bio}}" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

@endsection