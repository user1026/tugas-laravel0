@extends('layout.master')
@section('judul')
    Sign Up Form
@endsection

@section('content')
    
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
    <form action="/welcome0" method="post">
    @csrf
    <label>First Name:</label><br><br>
        <input type="text" name="dnama"><br><br>
        
        <label>Last Name:</label><br><br>
        <input type="text" name="bnama"><br><br>
        
        <label>Gender</label><br><br>
        <label><input type="radio">Male</label><br>
        <label><input type="radio">Female</label><br>
        <label><input type="radio">Other</label><br><br>

        <label for="nationality">Nationality</label><br><br>   
        <select name="nationality" id="">
        <option value="id">Indonesia</option>
        <option value="us">Amerika Serikat</option>
        <option value="eng">Inggris</option>
        </select><br><br>
        
        <label for="ls">Language Spoken</label><br><br>
        <label for="ls-bahasa"><input type="checkbox">Bahasa Indonesia</label><br>
        <label for="ls-eng"><input type="checkbox">English</label><br>
        <label for="ls-other"><input type="checkbox">Other</label><br><br>
        
        <label>Bio</label><br><br>
        <textarea style="margin: 0px; width: 321px; height: 120px;"></textarea><br>
        <input type="submit" value="signup">
    </form>
@endsection